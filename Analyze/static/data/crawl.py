# importing the requests library
import requests
import re
import time
import json

feedback = []

def scan_res_id(url):
    headers = {'Content-Type': "application/json", 'Accept': "application/json, text/plain, *",
               'X-Requested-With': "XMLHttpRequest",
               'User-Agent': "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36"}
    data = requests.get(url, headers)
    x = re.search('action=res&amp;id=(\d+)', data.text)
    return x[1]


def scan_data(resid, Last_Id=""):
    if Last_Id != "":
        URL = "https://www.foody.vn/__get/Review/ResLoadMore?ResId="+resid+"&LastId="+str(Last_Id)
        headers = {'Content-Type': "application/json", 'Accept': "application/json, text/plain, *",
                   'X-Requested-With': "XMLHttpRequest",
                   'User-Agent': "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36"}
        data = requests.get(URL, headers)
        comment = data.json()["Items"]
        str(comment).replace("[", "").replace("]", "")
        # print(comment[0]["Description"])
        for index, value in enumerate(comment):
            feedback.append(comment[index]["Description"])
        last_id = data.json()["LastId"]
        time.sleep(1)
        return last_id
    else:
        URL = "https://www.foody.vn/__get/Review/ResLoadMore?ResId="+str(resid)
        headers = {'Content-Type': "application/json", 'Accept': "application/json, text/plain, *",
                   'X-Requested-With': "XMLHttpRequest",
                   'User-Agent': "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36"}
        data = requests.get(URL, headers)
        comment = data.json()["Items"]
        str(comment).replace("[", "").replace("]", "")
        # print(comment[0]["Description"])
        for index, value in enumerate(comment):
            feedback.append(comment[index]["Description"])
        last_id = data.json()["LastId"]
        time.sleep(1)
        return last_id


def get_data(resid, Last_Id):
    id = scan_data(resid, Last_Id)
    # print(id)
    if id == Last_Id:
        return 0
    else:
        return get_data(resid,id)

def main_crawl(url):
    id_res = scan_res_id(url)
    line = get_data(id_res, "")
    if line not in feedback:
        feedback.append(line)
        with open('/home/truongson/TS/demoday/Analyze/static/data/target.txt', 'w') as target:
            for i in feedback:
                try:
                    if i != '':
                        i = i.replace('\n', '', 10)
                        target.write(i + '\n')
                except:
                    continue
    return feedback



