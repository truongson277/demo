import os, glob, json
from underthesea import chunk
from keras.models import load_model
import numpy as np
import time
from threading import Thread
import threading
os.environ['TF_CPP_MIN_LOG_LEVEL'] ='2'

word_dic_gob= { "_MAX":0 }
root_dir = ""
dic_file = root_dir + "demoday/Analyze/static/data/review-dic.json"
global max_words_gob
global max_words_space
global max_words_quality


def main(feedback):
    start_time = time.time()
    global max_words_gob
    global max_words_space
    global max_words_quality
    global word_dic_gob
    global word_dic_space
    global word_dic_quality
    # word_dic_gob= json.load(open(dic_file))
    word_dic_gob = json.load(open('/home/truongson/TS/demoday/Analyze/static/data/gob/review-dic-gob.json'))
    word_dic_space = json.load(open('/home/truongson/TS/demoday/Analyze/static/data/space/review-dic-space.json'))
    word_dic_quality = json.load(open('/home/truongson/TS/demoday/Analyze/static/data/quality/review-dic-quality.json'))
    max_words_gob = word_dic_gob['_MAX']
    max_words_space = word_dic_space['_MAX']
    max_words_quality = word_dic_quality['_MAX']
    very_good_gob1 = 0
    good_gob1 = 0
    medium_gob1 = 0
    bad_gob1 = 0
    other_gob1 = 0
    rate_space = 0
    very_good_space1 = 0
    good_space1 = 0
    medium_space1 = 0
    bad_space1 = 0
    other_space1 = 0
    # very_good = 0
    # good = 0
    # bad = 0
    # medium = 0
    # other = 0
    results_json_gob = {
        'very_good': 0,
        'good': 0,
        'medium': 0,
        'bad': 0,
        'other': 0
    }
    results_json_space = {
        'very_good': 0,
        'good': 0,
        'medium': 0,
        'bad': 0,
        'other': 0
    }
    # normal = {}
    # results = {
    #     'good': {},
    #     'bad': {},
    #     'normal': {}
    # }
    # g = 0
    # b = 0
    # n = 0
    # for i in f:
    #     i = i.replace('\n', '', 1000)
    #     if i != '':
    #         words = data_chunk(i)
    #         wt = "/".join(words)
    #         x = count_file_freq(wt)
    #
    #         model = model_load()
    #         result = model.predict(np.array([x]))
    #         ranks = np.arange(1, 4).reshape(3, 1)
    #         predicted_rank = result.dot(ranks).flatten()
    #         # print(i)
    #         # print(wt)
    #         # print(predicted_rank[0])
    #         if predicted_rank[0] > 2.5:
    #             # print('Class = Normal')
    #             results['normal'].update({
    #                 n: {
    #                     'text': i,
    #                     'chunk': wt
    #                 }
    #             })
    #             n += 1
    #         elif predicted_rank[0] > 1.8:
    #             # print('Class = Good')
    #             results['good'].update({
    #                 g: {
    #                     'text': i,
    #                     'chunk': wt
    #                 }
    #             })
    #             g += 1
    #         else:
    #             # print('Class = Bad')
    #             results['bad'].update({
    #                 b: {
    #                     'text': i,
    #                     'chunk': wt
    #                 }
    #             })
    #             b += 1
    # f.close()
    # print(results)
    # return results
    # with open('target.txt', 'r') as target:
    #     for index, value in enumerate(target):
    #         words = data_chunk(value)
    #         # print(words)
    #         _class = 0
    #         if len(words) >= 2:
    #             A = []
    #             B = []
    #             for i, val in enumerate(words):
    #                 if val[1] == 'R' or val[1] == 'N' or val[1] == 'A':
    #                     # print(val)
    #                     test = words.index(val)
    #                     try:
    #                         if words[test + 1][1] == 'A' or words[test + 1][1] == 'V':
    #                             word = words[test][0] + ' ' + words[test + 1][0]
    #                             if word not in A:
    #                                 A.append(word)
    #                             if words[test + 2][1] in ['A', 'R']:
    #                                 word2 = words[test][0] + ' ' + words[test + 1][0] + ' ' + words[test + 2][0]
    #                                 if word2 not in B:
    #                                     B.append(word2)
    #                     except:
    #                         continue
    #             wt = '/'.join(A+B)
    #             x = count_file_freq(wt)
    #             model = model_load_gob()
    #             if 1 in x:
    #                 result = model.predict(np.array([x]), batch_size=None, verbose=0, steps=None)
    #                 ranks = np.arange(1, 5).reshape(4, 1)
    #                 predicted_rank = result.dot(ranks).flatten()
    #                 # print(wt)
    #                 # print(x)
    #                 print(predicted_rank[0])
    #                 if predicted_rank[0] > 3.7:
    #                     very_good += 1
    #                 elif predicted_rank[0] > 2.7:
    #                     good += 1
    #                 elif predicted_rank[0] > 1.7:
    #                     medium += 1
    #                 else:
    #                     bad += 1
    #             else:
    #                 other += 1
    #                 print(0)
    space_result = []
    quality_result = []
    results_json_gob = {}
    results_json_space = {}
    results_json_quality = {}
    for index, value in enumerate(feedback):
        try:
            words = data_chunk(value)
            # print(words)
            _class = 0
            if len(words) >= 2:
                A = []
                B = []
                for i, val in enumerate(words):
                    if val[1] == 'R' or val[1] == 'N' or val[1] == 'A':
                        # print(val)
                        test = words.index(val)
                        try:
                            if words[test + 1][1] == 'A' or words[test + 1][1] == 'V':
                                word = words[test][0] + ' ' + words[test + 1][0]
                                if word not in A:
                                    A.append(word)
                                if words[test + 2][1] in ['A', 'R']:
                                    word2 = words[test][0] + ' ' + words[test + 1][0] + ' ' + words[test + 2][0]
                                    if word2 not in B:
                                        B.append(word2)
                        except:
                            continue
                wt = '/'.join(A + B)
                # x = count_file_freq(wt)
                # if 1 in x:
                #     predicted_rank_gob = gob(x)
                #     # print(wt)
                #     # print(x)
                #     print(predicted_rank_gob)
                #     if predicted_rank_gob > 4.0:
                #         very_good += 1
                #     elif predicted_rank_gob > 2.7:
                #         good += 1
                #     elif predicted_rank_gob > 1.7:
                #         medium += 1
                #     else:
                #         bad += 1
                # else:
                #     other += 1
                #     print(0)
                # try:
                #     t1 = threading.Thread(target=gob, args=(wt,))
                #     t2 = threading.Thread(target=space, args=(wt,))
                #     t1.start()
                #     t2.start()
                #     results_json_gob = t1.join()
                #     results_json_space = t2.join()
                #     # print("done in ", time.time() - t)
                # except:
                #     continue
                very_good_gob, good_gob, medium_gob, bad_gob, other_gob = gob(wt)
                very_good_gob1 += very_good_gob
                good_gob1 += good_gob
                medium_gob1 += medium_gob
                bad_gob1 += bad_gob
                other_gob1 += other_gob
                if space(wt) != 0:
                    space_get = space(wt)
                    space_result.append(space_get)
                # if quality(wt) != 0:
                #     quality_get = quality(wt)
                #     quality_result.append(quality_get)

                # very_good_space, good_space, medium_space, bad_space, other_space = space(wt)
                # very_good_space1 += very_good_space
                # good_space1 += good_space
                # medium_space1 += medium_space
                # bad_space1 += bad_space
                # other_space1 += other_space


        except:
            continue
    results_json_gob.update({
        'very_good': very_good_gob1,
        'good': good_gob1,
        'medium': medium_gob1,
        'bad': bad_gob1,
        'other': other_gob1
    })
    _len = len(space_result)
    if _len == 0:
        _len = 1
    results_json_space.update({
        'space_avg': sum(space_result)/_len*2.5
    })
    # results_json_quality.update({
    #     'quality_avg': sum(quality_result) / len(quality_result) * 3,
    # })
    # print('Time:', time.time() - start_time)
    return results_json_gob, results_json_space


def gob(wt):
    very_good = 0
    good = 0
    bad = 0
    medium = 0
    other = 0
    x = count_file_freq_gob(wt)
    model_gob = model_load_gob()
    result = model_gob.predict(np.array([x]), batch_size=None, verbose=0, steps=None)
    ranks = np.arange(1, 5).reshape(4, 1)
    predicted_rank_gob = result.dot(ranks).flatten()
    if 1 in x:
        # print(wt)
        # print(x)
        print(predicted_rank_gob)
        if predicted_rank_gob[0] > 4.0:
            very_good += 1
        elif predicted_rank_gob[0] > 2.7:
            good += 1
        elif predicted_rank_gob[0] > 1.7:
            medium += 1
        else:
            bad += 1
    else:
        other += 1
        print(0)
    return very_good, good, medium, bad, other


def space(wt):
    very_good = 0
    good = 0
    bad = 0
    medium = 0
    other = 0
    x = count_file_freq_space(wt)
    model_space = model_load_space()
    result = model_space.predict(np.array([x]), batch_size=None, verbose=0, steps=None)
    ranks = np.arange(1, 5).reshape(4, 1)
    predicted_rank_space = result.dot(ranks).flatten()
    if 1 in x:
        # print(wt)
        # print(x)
        print(predicted_rank_space)
        # if predicted_rank_space[0] > 4.0:
        #     very_good += 1
        # elif predicted_rank_space[0] > 2.7:
        #     good += 1
        # elif predicted_rank_space[0] > 1.7:
        #     medium += 1
        # else:
        #     bad += 1
        return predicted_rank_space[0]
    else:
        # other += 1
        # print(0)
        return 0

    # return very_good, good, medium, bad, other


def quality(wt):
    very_good = 0
    good = 0
    bad = 0
    medium = 0
    other = 0
    x = count_file_freq_quality(wt)
    model_quality = model_load_quality()
    result = model_quality.predict(np.array([x]), batch_size=None, verbose=0, steps=None)
    ranks = np.arange(1, 5).reshape(4, 1)
    predicted_rank_space = result.dot(ranks).flatten()
    if 1 in x:
        # print(wt)
        # print(x)
        # print(predicted_rank_space)
        # if predicted_rank_space[0] > 4.0:
        #     very_good += 1
        # elif predicted_rank_space[0] > 2.7:
        #     good += 1
        # elif predicted_rank_space[0] > 1.7:
        #     medium += 1
        # else:
        #     bad += 1
        return predicted_rank_space[0]
    else:
        # other += 1
        # print(0)
        return 0

    # return very_good, good, medium, bad, other


def model_load_gob():
    # filename = "Review/static/data/model_news.h5"
    filename = "/home/truongson/TS/demoday/Analyze/static/data/gob/model_gob.h5"
    loaded_model = load_model(filename)
    # loaded_model = pickle.load(open(filename, 'rb'))
    return loaded_model


def model_load_space():
    # filename = "Review/static/data/model_news.h5"
    filename = "/home/truongson/TS/demoday/Analyze/static/data/space/model_space.h5"
    loaded_model = load_model(filename)
    # loaded_model = pickle.load(open(filename, 'rb'))
    return loaded_model


def model_load_quality():
    # filename = "Review/static/data/model_news.h5"
    filename = "/home/truongson/TS/demoday/Analyze/static/data/quality/model_quality.h5"
    loaded_model = load_model(filename)
    # loaded_model = pickle.load(open(filename, 'rb'))
    return loaded_model


def data_chunk(text):
    _list = chunk(text)
    results_chunk = []
    for index, val in enumerate(_list):
        # if val[1] not in ['R', 'A']: continue
        results_chunk.append(val)
    return results_chunk


def text_to_ids(text):
    global word_dic_gob
    text = text.strip()
    text = text.replace('\n', '/')
    words = text.split("/")
    result = []
    for n in words:
        n = n.strip()
        if n == "": continue
        if not n in word_dic_gob: continue
            # if word_dic_gob["_MAX"] > cnt_max:continue
            # wid = word_dic_gob[n] = word_dic_gob["_MAX"]
            # word_dic_gob["_MAX"] += 1
        else:
            wid = word_dic_gob[n]
        result.append(wid)
    return result


def count_file_freq_gob(text):
    global max_words_gob
    cnt = [0 for n in range(max_words_gob)]
    text = text.strip()
    ids = text_to_ids(text)
    for wid in ids:
        cnt[wid] += 1
    return cnt


def count_file_freq_space(text):
    global max_words_space
    cnt = [0 for n in range(max_words_space)]
    text = text.strip()
    ids = text_to_ids(text)
    for wid in ids:
        cnt[wid] += 1
    return cnt


def count_file_freq_gob(text):
    global max_words_gob
    cnt = [0 for n in range(max_words_gob)]
    text = text.strip()
    ids = text_to_ids(text)
    for wid in ids:
        cnt[wid] += 1
    return cnt


def count_file_freq_quality(text):
    global max_words_quality
    cnt = [0 for n in range(max_words_quality)]
    text = text.strip()
    ids = text_to_ids(text)
    for wid in ids:
        cnt[wid] += 1
    return cnt


