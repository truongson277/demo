from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from Analyze.static.data.crawl import main_crawl
from Analyze.static.data.analyze import main
import Analyze.templates.realtimeapp
import time
import os
from keras import backend as K
K.clear_session()
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
# Create your views here.
@csrf_exempt
def index(request):
    return render(request, 'Analyze.templates.realtimeapp .index.html')



@csrf_exempt
def analyze_results(request):
    results = {}
    if request.GET['test'] != '':
        # url = 'https://www.foody.vn/da-nang/choi-s-kitchen-mon-an-han-quoc-chau-thi-vinh-te'
        url = 'https://www.foody.vn/da-nang/' + request.GET['test']
        # url = request.GET['test']
        feedback = main_crawl(url)
        array = []
        for i in feedback:
            if i not in array:
                array.append(i)
        result_gob, result_space = main(array)

        results.update({
            'gob': result_gob,
            'space': result_space
        })

        return JsonResponse(results, status=200)


# url = 'https://www.foody.vn/da-nang/choi-s-kitchen-mon-an-han-quoc-chau-thi-vinh-te'
#
# start_time = time.time()
# feedback = main_crawl(url)
# array = []
# for i in feedback:
#     if i not in array:
#         array.append(i)
# result_gob, result_space = main(array)
# print(time.time() - start_time)
